package com.mut.ntp;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mut.ntpdemo.R;

/**
 * 
 * @author MutMap
 * 
 */
public class MainActivity extends Activity {

	/**
	 * the button to get ntp mannually
	 */
	private Button mBtnGetNtp;

	/**
	 * Next ntp ocurr
	 */
	private TextView mTvNextNtp;

	/**
	 * custom digital clock
	 */
	private CustomTextClock mTvClock;

	/**
	 * 
	 */

	private CustomAnalogClock mAnologClock;
	/**
	 * CountdownTimer use to fetch ntp every 10 minutes
	 * 
	 */
	private CountDownTimer mTimer;
	/**
	 * 10 mins
	 */
	private final static long COUNT_DOWN_TIME = 10 * 60 * 1000;

	/**
	 * 1 second
	 */
	private final static long COUNT_DOWN_INTERVAL = 1000;

	/**
	 * NTP Server
	 */
	private static final String SERVER_NTP = "0.pool.ntp.org";

	/**
	 * analog clock dial backgrounds
	 */
	private static final int[] ANALOG_DIAL_BG = new int[] { R.drawable.red_bg, R.drawable.green_bg, R.drawable.blue_bg, R.drawable.buble_bg };

	/**
	 * request timeout
	 */
	private static final int TIME_OUT = 30 * 1000;

	private SNTPTask mTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTvNextNtp = (TextView) findViewById(R.id.tv_time_to_next_ntp);

		mTvClock = (CustomTextClock) findViewById(R.id.tv_text_clock);

		mAnologClock = (CustomAnalogClock) findViewById(R.id.view_analog_clock);

		mTimer = new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_INTERVAL) {

			@Override
			public void onTick(long millisUntilFinished) {
				// update view
				mTvNextNtp.setText(String.format("%d min %d sec", 
						TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
						TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

			}

			@Override
			public void onFinish() {
				// re fetch NTP
				executeNtp();
			}
		};

		mBtnGetNtp = (Button) findViewById(R.id.btn_get_ntp);
		mBtnGetNtp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// cancel the timer
				mTimer.cancel();
				// cancel current running task;
				mTask.cancel(true);
				// start sntp task
				executeNtp();
				// disable onclick again
				// mBtnGetNtp.setOnClickListener(null);
			}
		});

		executeNtp();

	}

	private void executeNtp() {
		mTask = new SNTPTask();
		mTask.execute(SERVER_NTP);
	}

	/**
	 * The worker asynctask to fetch ntp value in background, using SNTP from
	 * android input: the ntp server address output: the value of ntp
	 * 
	 * @author MutMap
	 * 
	 */
	private class SNTPTask extends AsyncTask<String, Void, Long> {

		public SNTPTask() {
		}

		@Override
		protected Long doInBackground(String... params) {
			String server = params[0];
			SntpClient client = new SntpClient();
			if (client.requestTime(server, TIME_OUT)) {
				return client.getNtpTime() + SystemClock.elapsedRealtime() - client.getNtpTimeReference();
			} else {
				return 0L;
			}
		}

		@Override
		protected void onPostExecute(Long result) {

			if (isCancelled())
				return;

			// if we failed to get ntp , do nothing
			if (result == 0L) {

			} else { // set to the view
				Random rnd = new Random();

				int color = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

				mTvClock.setTextColor(color);
				mAnologClock.setDialDwable(ANALOG_DIAL_BG[rnd.nextInt(4)]);

				mAnologClock.setMilisecond(result);
				mTvClock.setMilisecond(result);
			}
			Log.d("ntp", String.valueOf(result));
			// start the timer
			mTimer.start();
		}

	}
}
