package com.mut.ntp;

import java.util.Calendar;

import android.content.Context;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextClock extends TextView {

	private final static String FORMAT = "H:mm:ss";

	private long mMilisecond;

	private Calendar mTime;

	private boolean mAttached;

	public void setMilisecond(long mil) {
		getHandler().removeCallbacks(mTicker);
		mMilisecond = mil;
		mTicker.run();
	}

	public CustomTextClock(Context context) {
		super(context);
		init();
	}

	public CustomTextClock(Context context, AttributeSet set) {
		super(context, set);
		init();
	}

	private void init() {
		mTime = Calendar.getInstance();
		mMilisecond = System.currentTimeMillis();
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!mAttached) {
			mAttached = true;
			mMilisecond = System.currentTimeMillis();
			mTicker.run();
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		if (mAttached) {
			getHandler().removeCallbacks(mTicker);

			mAttached = false;
		}
	}

	private final Runnable mTicker = new Runnable() {
		public void run() {
			onTimeChanged();
			mMilisecond += 1000;
			long now = SystemClock.uptimeMillis();
			long next = now + (1000 - now % 1000);
			getHandler().postAtTime(mTicker, next);
		}
	};

	private void onTimeChanged() {
		mTime.setTimeInMillis(mMilisecond);
		setText(DateFormat.format(FORMAT, mTime));
	}

}
