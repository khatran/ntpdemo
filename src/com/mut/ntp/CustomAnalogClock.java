package com.mut.ntp;

import java.util.Calendar;

import com.mut.ntpdemo.R;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AnalogClock;

public class CustomAnalogClock extends View {
	private Time mTime;

	private Drawable mSecondHand;
	private Drawable mHourHand;
	private Drawable mMinuteHand;
	private Drawable mDial;
	
	private long mMilisecond;

	private int mDialWidth;
	private int mDialHeight;

	private boolean mAttached;

	
	private float mMinutes ;
	private float mHour ;
	private float mSecond;
	private boolean mChanged;

	public CustomAnalogClock(Context context) {
		this(context, null);
	}

	public CustomAnalogClock(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CustomAnalogClock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
//		Resources r = context.getResources();
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomAnalogClock, defStyle, 0);

		mDial = a.getDrawable(R.styleable.CustomAnalogClock_dial);
		// if (mDial == null) {
		// }

		mHourHand = a.getDrawable(R.styleable.CustomAnalogClock_hour);
		// if (mHourHand == null) {
		// mHourHand =
		// }

		mMinuteHand = a.getDrawable(R.styleable.CustomAnalogClock_min);
		
		mSecondHand = a.getDrawable(R.styleable.CustomAnalogClock_second);
		// if (mMinuteHand == null) {
		// mMinuteHand =
		// }

		mTime = new Time();
		mMilisecond = System.currentTimeMillis();

		mDialWidth = mDial.getIntrinsicWidth();
		mDialHeight = mDial.getIntrinsicHeight();
		
		
	}

	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		float hScale = 1.0f;
		float vScale = 1.0f;

		if (widthMode != MeasureSpec.UNSPECIFIED && widthSize < mDialWidth) {
			hScale = (float) widthSize / (float) mDialWidth;
		}

		if (heightMode != MeasureSpec.UNSPECIFIED && heightSize < mDialHeight) {
			vScale = (float) heightSize / (float) mDialHeight;
		}

		float scale = Math.min(hScale, vScale);

		setMeasuredDimension(resolveSize((int) (mDialWidth * scale), widthMeasureSpec), resolveSize((int) (mDialHeight * scale), heightMeasureSpec));
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		mChanged = true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		boolean changed = mChanged;
		if (changed) {
			mChanged = false;
		}

		int availableWidth = getWidth();
		int availableHeight = getHeight();

		int x = availableWidth / 2;
		int y = availableHeight / 2;

		final Drawable dial = mDial;
		int w = dial.getIntrinsicWidth();
		int h = dial.getIntrinsicHeight();

		boolean scaled = false;

		if (availableWidth < w || availableHeight < h) {
			scaled = true;
			float scale = Math.min((float) availableWidth / (float) w, (float) availableHeight / (float) h);
			canvas.save();
			canvas.scale(scale, scale, x, y);
		}

		if (changed) {
			dial.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
		}
		dial.draw(canvas);

		canvas.save();
		canvas.rotate(mHour / 12.0f * 360.0f, x, y);
		final Drawable hourHand = mHourHand;
		if (changed) {
			w = hourHand.getIntrinsicWidth();
			h = hourHand.getIntrinsicHeight();
			hourHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
		}
		hourHand.draw(canvas);
		canvas.restore();

		canvas.save();
		canvas.rotate(mMinutes / 60.0f * 360.0f, x, y);

		final Drawable minuteHand = mMinuteHand;
		if (changed) {
			w = minuteHand.getIntrinsicWidth();
			h = minuteHand.getIntrinsicHeight();
			minuteHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
		}
		minuteHand.draw(canvas);
		canvas.restore();
		
		canvas.save();
		canvas.rotate(mSecond / 60.0f * 360.0f, x, y);

		
		final Drawable secondHand = mSecondHand;
		if (changed) {
			w = secondHand.getIntrinsicWidth();
			h = secondHand.getIntrinsicHeight();
			secondHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
		}
		secondHand.draw(canvas);
		canvas.restore();

		if (scaled) {
			canvas.restore();
		}
	}

	private void onTimeChanged() {
		mTime.set(mMilisecond);

		int hour = mTime.hour;
		int minute = mTime.minute;
		int second = mTime.second;

		
		mSecond = second;
		mMinutes = minute + second / 60.0f;
		mHour = hour + mMinutes / 60.0f;
		mChanged = true;
		invalidate();
	}
	
	
	

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!mAttached) {
			mAttached = true;
			mMilisecond = System.currentTimeMillis();
			mTicker.run();
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		if (mAttached) {
			getHandler().removeCallbacks(mTicker);

			mAttached = false;
		}
	}

	private final Runnable mTicker = new Runnable() {
		public void run() {
			onTimeChanged();
			mMilisecond += 1000;
			long now = SystemClock.uptimeMillis();
			long next = now + (1000 - now % 1000);
			getHandler().postAtTime(mTicker, next);
		}
	};

	public void setMilisecond(long mil) {
		getHandler().removeCallbacks(mTicker);
		mMilisecond = mil;
		mTicker.run();
	}
	
	public void setDialDwable(int resource){
		mDial = getResources().getDrawable(resource);
		invalidate();
	}
	

}